# sql-practice

Language: Polish. PDF reports of my exercises in SQL, as part of the Databases subject on my CS studies. From the absolute basics, up to procedures, triggers and pattern searching.